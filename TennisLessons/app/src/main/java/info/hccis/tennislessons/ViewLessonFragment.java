package info.hccis.tennislessons;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import info.hccis.tennislessons.bo.TennisLessonsBO;
import info.hccis.tennislessons.bo.TennisLessonsViewModel;
import info.hccis.tennislessons.databinding.FragmentViewLessonBinding;

/**
 * View Lesson Fragment
 *
 * @author CIS2250
 * @since 20220118
 * @modified by mariannahollanda
 * @since 20220119
 */
public class ViewLessonFragment extends Fragment {
    private FragmentViewLessonBinding binding;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentViewLessonBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TennisLessonsViewModel tennisLessonsViewModel = new ViewModelProvider(getActivity()).get(TennisLessonsViewModel.class);

        //Bundle is accessed to get the ticket order which is passed from the add order fragment.
        Bundle bundle = getArguments();
        TennisLessonsBO tennisLessonsBO = (TennisLessonsBO) bundle.getSerializable(AddLessonFragment.KEY);
        Log.d("ViewLessonFragment MH", "Lesson passed in: " + tennisLessonsBO.toString());

        //Build the output to be displayed in the textview
        String output = "";
        double total = 0;
        for (TennisLessonsBO lesson : tennisLessonsViewModel.getTennisLessons()) {
            output += lesson.toString() + System.lineSeparator();
            total += lesson.calculateCost();
        }
        output += System.lineSeparator() + "Total: $" + total;
        binding.textviewLessonDetails.setText(output);

        //Button sends the user back to the add fragment
        binding.buttonAddOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(ViewLessonFragment.this)
                        .navigate(R.id.action_ViewLessonFragment_to_AddLessonFragment);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
