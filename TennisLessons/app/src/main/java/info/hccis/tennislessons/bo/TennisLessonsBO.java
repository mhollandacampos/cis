package info.hccis.tennislessons.bo;

import java.io.Serializable;
import java.util.Scanner;

/**
 * Business logic class for tennis lessons
 *
 * @author mariannahollanda
 * @since 20220119
 */
public class TennisLessonsBO implements Serializable {

    public static final double PRIVATE_MEMBER = 55;
    public static final double TWO_GROUP_MEMBER = 30;
    public static final double THREE_GROUP_MEMBER = 21;
    public static final double FOUR_GROUP_MEMBER = 16;

    public static final double PRIVATE_NON_MEMBER = 60;
    public static final double TWO_GROUP_NON_MEMBER = 33;
    public static final double THREE_GROUP_NON_MEMBER = 23;
    public static final double FOUR_GROUP_NON_MEMBER = 18;

    private boolean isMember;
    private int groupNumber;
    private int hours;
    private String isMemberYN;
    private double cost;

    public TennisLessonsBO(int groupNumber, String isMemberYN, int hours) {
        this.groupNumber = groupNumber;
        this.isMemberYN = isMemberYN;
        this.hours = hours;
    }

    /**
     * Get information from the user
     *
     * @author mariannahollanda
     * @since 20220119
     */
    public void getInformation() {
        System.out.println("Welcome to the CIS Tennis Program");
        Scanner input = new Scanner(System.in);

        System.out.println("How many are in the group? (1,2,3,4)");
        groupNumber = input.nextInt();
        input.nextLine();

        System.out.println("Are you a member? (Y/N)");
        isMemberYN = input.nextLine();

        isMember = isMemberYN.equalsIgnoreCase("Y");

        System.out.println("How many hours do you want for your lesson?");
        hours = input.nextInt();
        input.nextLine();
    }

    /**
     * Calculate the cost
     *
     * @return
     * @author mariannahollanda
     * @since 20220119
     */
    public double calculateCost() {

        switch (isMemberYN) {
            case "Y":
            case "y":
                if (groupNumber == 1) {
                    cost = hours * PRIVATE_MEMBER;
                } else if (groupNumber == 2) {
                    cost = hours * TWO_GROUP_MEMBER;
                } else if (groupNumber == 3) {
                    cost = hours * THREE_GROUP_MEMBER;
                } else if (groupNumber == 4) {
                    cost = hours * FOUR_GROUP_MEMBER;
                }
                break;
            case "N":
            case "n":
                if (groupNumber == 1) {
                    cost = hours * PRIVATE_NON_MEMBER;
                } else if (groupNumber == 2) {
                    cost = hours * TWO_GROUP_NON_MEMBER;
                } else if (groupNumber == 3) {
                    cost = hours * THREE_GROUP_NON_MEMBER;
                } else if (groupNumber == 4) {
                    cost = hours * FOUR_GROUP_NON_MEMBER;
                }
                break;
        }
        return cost;
    }

    public boolean isMember() {
        return isMember;
    }

    public void setMember(boolean member) {
        isMember = member;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public String getIsMemberYN() {
        return isMemberYN;
    }

    public void setIsMemberYN(String isMemberYN) {
        this.isMemberYN = isMemberYN;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void display() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "Lesson details - Lesson group size: " + groupNumber + " Member: " + isMemberYN + "/hour Cost: $" + calculateCost();
    }

}


