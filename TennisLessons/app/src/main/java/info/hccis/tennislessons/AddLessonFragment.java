package info.hccis.tennislessons;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import info.hccis.tennislessons.bo.TennisLessonsBO;
import info.hccis.tennislessons.bo.TennisLessonsViewModel;
import info.hccis.tennislessons.databinding.FragmentAddLessonBinding;

/**
 * Add Lesson Fragment
 *
 * @author CIS2250
 * @since 20220118
 * @modified by mariannahollanda
 * @since 20220119
 */
public class AddLessonFragment extends Fragment {
    public static final String KEY = "info.hccis.tennislessons.LESSON";
    private FragmentAddLessonBinding binding;
    TennisLessonsBO tennisLessonsBO;
    TennisLessonsViewModel tennisLessonsViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("AddLessonFragment MH", "onCreateView triggered");
        binding = FragmentAddLessonBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("AddLessonFragment MH", "onViewCreated triggered");
        tennisLessonsViewModel = new ViewModelProvider(getActivity()).get(TennisLessonsViewModel.class);
        binding.buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("AddLessonFragment MH", "Submit was triggered");
                try {
                    calculate();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(KEY, tennisLessonsBO);
                    NavHostFragment.findNavController(AddLessonFragment.this)
                            .navigate(R.id.action_AddLessonFragment_to_ViewLessonFragment, bundle);
                } catch (Exception e) {
                    Log.d("AddLessonFragment MH", "Error calculating: " + e.getMessage());
                }
            }
        });
    }

    /**
     * Calculate the Lesson cost.
     *
     * @throws Exception Throw exception if number of tickets entered caused an issue.
     * @author CIS2250
     * @since 20220118
     * @modified by mariannahollanda
     * @since 20220119
     */
    public void calculate() throws Exception {
        Log.d("MH-MainActivity", "Lesson group size: " + binding.editTextGroupNumber.getText().toString());
        Log.d("MH-MainActivity", "Member: " + binding.editTextMember.getText().toString());
        Log.d("MH-MainActivity", "Submit button was clicked.");
        String isMember = binding.editTextMember.getText().toString();
        int groupSize;
        int totalHours;
        try {
            groupSize = Integer.parseInt(binding.editTextGroupNumber.getText().toString());
            totalHours = Integer.parseInt(binding.editTextHours.getText().toString());
        } catch (Exception e) {
            groupSize = 0;
            totalHours = 0;
        }
        tennisLessonsBO = new TennisLessonsBO(groupSize, isMember, totalHours);
        try {
            double cost = tennisLessonsBO.calculateCost();
            tennisLessonsViewModel.getTennisLessons().add(tennisLessonsBO);

            //Now that I have the cost, want to set the value on the textview.
            Locale locale = new Locale("en", "CA");
            DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
            DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(locale);
            decimalFormat.setDecimalFormatSymbols(dfs);
            String formattedCost = decimalFormat.format(cost);

        } catch (NumberFormatException nfe) {
            binding.editTextGroupNumber.setText("");
            throw nfe;
        } catch (Exception e) {
            binding.editTextGroupNumber.setText("");
            throw e;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}