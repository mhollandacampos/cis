package info.hccis.tennislessons.bo;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

/**
 * View Model for Tennis Lessons
 *
 * @author mariannahollanda
 * @since 20220119
 */
public class TennisLessonsViewModel extends ViewModel {
    private ArrayList<TennisLessonsBO> tennisLessons = new ArrayList();
    public ArrayList<TennisLessonsBO> getTennisLessons(){return tennisLessons;}
    public void setTennisLessons(ArrayList<TennisLessonsBO> tennisLessons){
        this.tennisLessons = tennisLessons;
    }
}
